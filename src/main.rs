/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

extern crate sdl2;
extern crate rand;

use std::env;
use std::path::Path;
use sdl2::image::{LoadTexture, INIT_PNG, INIT_JPG};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::keyboard::Scancode;
use sdl2::rect::Rect;
use sdl2::rect::Point;
use sdl2::pixels::Color;
use std::collections::HashSet;
use std::{thread, time};
use std::rc::Rc;
use rand::{thread_rng, Rng};

struct Entity<'a> {
    sprite: Rc<sdl2::render::Texture<'a>>,
    rect: sdl2::rect::Rect,
    x_vel: i32,
    y_vel: i32,
    scale: f32,
    rot: f64,
}

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let ttf_context = sdl2::ttf::init().unwrap();
    let _image_context = sdl2::image::init(INIT_PNG | INIT_JPG).unwrap();
    let window = video_subsystem.window("Rusteroids: Video", 800, 600)
      .position_centered()
      .resizable()
      .build()
      .unwrap();
    
    let mut events = sdl_context.event_pump().unwrap();
    
    let mut canvas = window.into_canvas().accelerated().present_vsync().build().unwrap();
    let texture_creator = canvas.texture_creator();
    let mut torpedoes: Vec<Entity> = Vec::new();
    let mut asteroids: Vec<Entity> = Vec::new();
    let torpedo_sprite = Rc::new(texture_creator.load_texture(Path::new("data/torpedo.png")).unwrap());
    let asteroid_sprite = Rc::new(texture_creator.load_texture(Path::new("data/asteroid.png")).unwrap());
    let bg = Rc::new(texture_creator.load_texture(Path::new("data/bg.jpg")).unwrap());
    
    let mut font = ttf_context.load_font(Path::new("data/fonts/FreeMono.ttf"), 128).unwrap();
    font.set_style(sdl2::ttf::STYLE_BOLD);
    
    let mut rng = thread_rng();
    
    let mut score = 0;
    let mut alive = true;
    let mut cooldown = 0;
    
    let mut player = Entity {
        sprite: Rc::new(texture_creator.load_texture(Path::new("data/ship.png")).unwrap()),
        rect: sdl2::rect::Rect::new(400, 300, 50, 50),
        x_vel: 0,
        y_vel: 0,
        scale: 1.0,
        rot: 0.0,
    };
    
    for x in 0..3 {
        
        let mut x_coord = 0;
        if rng.gen_range(0, 2) == 1 {
            x_coord = rng.gen_range(0, 100);
        } else {
            x_coord = rng.gen_range(600, 800);
        }
        
        let new_asteroid = Entity {
            sprite: asteroid_sprite.clone(),
            rect: sdl2::rect::Rect::new(x_coord, rng.gen_range(-50, 650), 50, 50),
            x_vel: rng.gen_range(-2, 2),
            y_vel: rng.gen_range(-2, 2),
            scale: 1.0,
            rot: player.rot,
        };
        
        asteroids.push(new_asteroid);
    }
    
    'mainloop: loop {
        
        for event in events.poll_iter() {
            match event {
                Event::Quit{..} |
                Event::KeyDown {keycode: Option::Some(Keycode::Escape), ..} =>
                    break 'mainloop,
                Event::KeyDown {keycode: Option::Some(Keycode::Up), ..} => {
                    player.x_vel += (player.rot.to_radians().sin() * 2.0) as i32;
                    player.y_vel += (player.rot.to_radians().cos() * 2.0) as i32;
                    
                    println!("{}, {}", player.x_vel, player.y_vel);
                },
                Event::KeyDown {keycode: Option::Some(Keycode::Space), ..} => {
                    if cooldown == 0 {
                        let new_torpedo = Entity {
                            sprite: torpedo_sprite.clone(),
                            rect: sdl2::rect::Rect::new(player.rect.x + 25, player.rect.y + 25, 5, 5),
                            x_vel: (player.rot.to_radians().sin() * 8.0) as i32,
                            y_vel: (player.rot.to_radians().cos() * 8.0) as i32,
                            scale: 1.0,
                            rot: player.rot,
                        };
                    
                        if alive {
                            torpedoes.push(new_torpedo);
                        }
                        
                        cooldown = 30;
                    }
                },
                
                Event::KeyDown {keycode: Option::Some(Keycode::R), ..} => {
                    score = 0;
                    alive = true;
                    asteroids.clear();
                    torpedoes.clear();
                    
                    for x in 0..3 {
                        let mut x_coord = 0;
                        if rng.gen_range(0, 2) == 1 {
                            x_coord = rng.gen_range(0, 100);
                        } else {
                            x_coord = rng.gen_range(600, 800);
                        }
                        
                        let new_asteroid = Entity {
                            sprite: asteroid_sprite.clone(),
                            rect: sdl2::rect::Rect::new(x_coord, rng.gen_range(0, 600), 50, 50),
                            x_vel: rng.gen_range(-3, 3),
                            y_vel: rng.gen_range(-3, 3),
                            scale: 1.0,
                            rot: player.rot,
                        };
        
                        asteroids.push(new_asteroid);
                    }
                    
                    player = Entity {
                        sprite: Rc::new(texture_creator.load_texture(Path::new("data/ship.png")).unwrap()),
                        rect: sdl2::rect::Rect::new(400, 300, 50, 50),
                        x_vel: 0,
                        y_vel: 0,
                        scale: 1.0,
                        rot: 0.0,
                    };
                    
                },
                
                _ => {}
            }
        }
        
        let keystate = events.keyboard_state();
        
        if keystate.is_scancode_pressed(Scancode::Left) {
            player.rot -= 3.0;
        } else if keystate.is_scancode_pressed(Scancode::Right) {
            player.rot += 3.0;
        }
        
        player.rect.x += player.x_vel;
        player.rect.y -= player.y_vel;
        
        if player.rect.x > 850 {
            player.rect.x = -50;
        } else if player.rect.x < -50 {
            player.rect.x = 850;
        }
        
        if player.rect.y > 650 {
            player.rect.y = -50;
        } else if player.rect.y < -50 {
            player.rect.y = 650;
        }
        
        if cooldown > 0 {
            cooldown -= 1;
        }
        
        canvas.clear();
        canvas.copy_ex(&bg, None, None, 0.0, None, false, false);
        
        
        //println!("{}", player.rot);
        if alive {
            canvas.copy_ex(&player.sprite, None, player.rect, player.rot, None, false, false);
        }
        
        let mut count: usize = 0;
        let mut to_remove: Vec<usize> = Vec::new();
        
        println!("Torpedoes active: {}", torpedoes.len());
        
        for torpedo in &mut torpedoes {
            torpedo.rect.x += torpedo.x_vel;
            torpedo.rect.y -= torpedo.y_vel;
            
            let mut ast_count: usize = 0;
            let mut ast_to_remove: Vec<usize> = Vec::new();
            
            for asteroid in &asteroids {
                if asteroid.rect.has_intersection(torpedo.rect) {
                    ast_to_remove.push(ast_count);
                    to_remove.push(count);
                    
                    break;
                }
                
                ast_count += 1;
            }
            
            ast_to_remove.sort();
            ast_to_remove.reverse();
            
            for ast in ast_to_remove {
                if asteroids[ast].scale > 0.25 {
                    for x in (0..3) {
                        let new_asteroid = Entity {
                            sprite: asteroid_sprite.clone(),
                            rect: sdl2::rect::Rect::new(asteroids[ast].rect.x, asteroids[ast].rect.y, asteroids[ast].rect.width() / 2, asteroids[ast].rect.height() / 2),
                            x_vel: rng.gen_range(-3, 3),
                            y_vel: rng.gen_range(-3, 3),
                            scale: asteroids[ast].scale / 2.0,
                            rot: player.rot,
                        };
        
                        asteroids.push(new_asteroid);
                    }
                }
                
                score += 50;
                
                if score % 1000 == 0 {
                    for x in 0..3 {
                        let mut x_coord = 0;
                        if rng.gen_range(0, 2) == 1 {
                            x_coord = rng.gen_range(0, 100);
                        } else {
                            x_coord = rng.gen_range(600, 800);
                        }
                        
                        let new_asteroid = Entity {
                            sprite: asteroid_sprite.clone(),
                            rect: sdl2::rect::Rect::new(x_coord, rng.gen_range(0, 600), 50, 50),
                            x_vel: rng.gen_range(-2, 2),
                            y_vel: rng.gen_range(-2, 2),
                            scale: 1.0,
                            rot: player.rot,
                        };
        
                        asteroids.push(new_asteroid);
                    }
                }
                
                asteroids.remove(ast);
            }
            
            if torpedo.rect.x > 850 || torpedo.rect.x < -50 || torpedo.rect.y > 650 || torpedo.rect.y < -50 {
                to_remove.push(count)
            } else {
                canvas.copy_ex(&torpedo.sprite, None, torpedo.rect, torpedo.rot, None, false, false);
            }
            
            count += 1;
        }
        
        to_remove.sort();
        to_remove.reverse();
        
        for count in to_remove {
            torpedoes.remove(count);
        }
        
        
        for asteroid in &mut asteroids {
            if asteroid.rect.has_intersection(player.rect) {
                alive = false;
            }
            
            asteroid.rect.x += asteroid.x_vel;
            asteroid.rect.y -= asteroid.y_vel;
            
            println!("X: {}, Y: {}", asteroid.rect.x, asteroid.rect.y);
            
            if asteroid.rect.x > 850 {
                asteroid.rect.x = -50;
            } else if asteroid.rect.x < -50 {
                asteroid.rect.x = 850;
            }
            
            if asteroid.rect.y > 650 {
                asteroid.rect.y = -50;
            } else if asteroid.rect.y < -50 {
                asteroid.rect.y = 650;
            }
            
            asteroid.rot += 1.0;
            canvas.copy_ex(&asteroid.sprite, None, asteroid.rect, asteroid.rot, None, false, false);
        }
        
        let score_surface = font.render(format!("Score: {}", score).as_str()).blended(Color::RGBA(255, 0, 0, 255)).unwrap();
        let score_texture = texture_creator.create_texture_from_surface(&score_surface).unwrap();
        canvas.copy(&score_texture, None, Rect::new(50, 50, 100, 50)).unwrap();
        
        if !alive {
            let score_surface = font.render(format!("You are dead. Press 'R' to restart.").as_str()).blended(Color::RGBA(255, 0, 0, 255)).unwrap();
            let score_texture = texture_creator.create_texture_from_surface(&score_surface).unwrap();
            canvas.copy(&score_texture, None, Rect::new(275, 250, 300, 50)).unwrap();
        }
        
        canvas.present();
    }
}
